package unacceptableuse.officebuilder.image;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.HashMap;

import unacceptableuse.officebuilder.OfficeBuilder;

public class Font {

	
	private static String fontChars = "ABCDEFGHIJKLMNOPQRSTUVWX" +
			                          "YZabcdefghijklmnopqrstuv" +
			                          "wxyz1234567890+-.!?,\"\\";

	private SpriteSheet sheet;
	private HashMap<String, Image>fontcache = new HashMap<String, Image>();
	private OfficeBuilder game;
	public Font(Graphics g, SpriteSheet sheet, OfficeBuilder game)
	{

		this.sheet = sheet;
		this.game = game;

		for(int i = 0; i < fontChars.length(); i++)
		{
			String character = String.valueOf(fontChars.charAt(i));
			fontcache.put(character, sheet.getSprite(i));
		}
	
		
	
	
			
		
	}
	/**
	 * 
	 * @param str The string to render
	 * @param x position on screen
	 * @param y position on screen
	 * @param col The hexidecimal colour of the text
	 * @param size The size in pixels
	 */
	public void drawString(String str, int x, int y, int col, int size, Graphics g)
	{

		for(int i = 0; i < str.length(); i++)
		{
			System.out.println(str.charAt(i));
			System.out.println(fontcache.get(String.valueOf(str.charAt(i))));
			g.drawImage(fontcache.get(String.valueOf(str.charAt(i))),x, y, size, size, game);
		}
	}
	
	
}




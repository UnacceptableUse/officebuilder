package unacceptableuse.officebuilder.image;

import java.awt.Image;
import java.awt.image.BufferedImage;

public class SpriteSheet {

	private BufferedImage sheet;
	private BufferedImage[] imagecache;
	
	
	public SpriteSheet(BufferedImage sheet, int spriteSize)
	{
		System.out.println("Loading sheet..");
		this.sheet = sheet;
		int rows = sheet.getWidth() / spriteSize;
		int cols = sheet.getHeight() / spriteSize;
		imagecache = new BufferedImage[spriteSize*spriteSize];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				imagecache[(i * cols) + j] = sheet.getSubimage(j * spriteSize,
						i * spriteSize, spriteSize, spriteSize);
			}
		}
		System.out.println("Loaded "+imagecache.length+" sprites.");
	}
	
	public Image getSprite(int index)
	{

		return imagecache[index];
	}
}

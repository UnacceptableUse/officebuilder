package unacceptableuse.officebuilder;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import unacceptableuse.officebuilder.image.Font;
import unacceptableuse.officebuilder.image.SpriteSheet;
import unacceptableuse.officebuilder.screen.Screen;

public class OfficeBuilder extends Canvas {

	public static String VERSION = "0.01";
	public static int WIDTH = 800, HEIGHT = 600, TICKS_PER_SECOND = 60;

	private Graphics g;
	private boolean gameRunning = true;
	private int tps, fps;
	private Screen currentScreen = new Screen(WIDTH, HEIGHT, this);
	public SpriteSheet sheetTerrain, sheetBackgrounds, sheetFont;
	public Font font;

	public OfficeBuilder(String[] args) {
		setSize(200, 200);
		setBounds(0, 0, 800, 600);

	}

	public void start() {
		try {
			gameLoop();
		} catch (Exception e) {
			// TODO error handler

			e.printStackTrace();
		}
	}

	private void gameLoop() {
		long lastTime = System.nanoTime();
		double unprocessed = 0;
		double nsPerTick = 1000000000.0 / TICKS_PER_SECOND;

		int ticks = 0;
		int frames = 0;
		long lastTimer1 = System.currentTimeMillis();

		init();

		while (gameRunning) {
			long now = System.nanoTime();
			unprocessed += (now - lastTime) / nsPerTick;
			lastTime = now;
			while (unprocessed >= 1) {
				ticks++;
				tick();
				unprocessed -= 1;

			}
			{
				frames++;
				render();
			}

			if (System.currentTimeMillis() - lastTimer1 > 1000) {
				lastTimer1 += 1000;
				System.out.println(frames + " fps, " + ticks + " ticks.");
				fps = frames;
				tps = ticks;
				ticks = 0;
				frames = 0;

			}

		}
	}

	private void render() {

		g.drawImage(sheetFont.getSprite(1), 20, 20, 32, 32, this);
	//currentScreen.render(g);
	}

	private void tick() {

	}

	private void init() {

		loadSpriteSheets();

	}

	private void loadSpriteSheets() {
		// sheetBackgrounds = new
		// SpriteSheet(ImageIO.read(this.getClass().getResource("backgrounds.png")),
		// 32);
		try {
			sheetFont = new SpriteSheet(ImageIO.read(this.getClass()
					.getResource("/res/font_1.png")), 32);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		font = new Font(g, sheetFont, this);
		// sheetTerrain = new
		// SpriteSheet(ImageIO.read(this.getClass().getResource("terrain.png")),
		// 32);
	}

	public static void main(String[] args) {
		OfficeBuilder game = new OfficeBuilder(args);
		JFrame frame = new JFrame("Office Builder v" + VERSION);
		frame.add(game, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
		frame.setIgnoreRepaint(true);
		frame.setResizable(false);
		frame.setFocusable(true);
		frame.setVisible(true);

		// frame.pack();

		frame.createBufferStrategy(3);

		BufferStrategy bs = frame.getBufferStrategy();
		game.g = bs.getDrawGraphics();
		
		game.start();
	}
}

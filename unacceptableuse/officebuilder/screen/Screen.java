package unacceptableuse.officebuilder.screen;

import java.awt.Graphics;
import java.awt.Image;

import unacceptableuse.officebuilder.OfficeBuilder;
import unacceptableuse.officebuilder.menu.Menu;

public class Screen {

	protected Menu currentMenu = null;
	protected OfficeBuilder game;
	
	public Screen(int width, int height, OfficeBuilder ob)
	{
		game = ob;
	}
	
	public void render(Graphics g)
	{
		game.font.drawString("test", 10, 10, 0x0000, 32, g);
	}
	
	public void drawBackgroundImage(Image img, Graphics g)
	{
		
	}
}
